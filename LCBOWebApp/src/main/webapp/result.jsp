<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<body>
<center>
<h1 class="page-header">
    Available Brands
</h1>
<%
List result= (List) request.getAttribute("brands");
Iterator it = result.iterator();
out.println("<br>At LCBO, We have <br><br>");
while(it.hasNext()){
out.println(it.next()+"<br>");
}
%>
<br>
<form method="get" action="GoBack">
<button type="submit" value="Go back" class="btn btn-primary">Go Back</button>
</form>
</body>
</html>